<?php declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Client implements ClientInterface
{
    private HttpClientInterface $client;
    private string $responseCode = '';

    public function __construct()
    {
        $this->client = HttpClient::create();
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @throws ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | \JsonException
     */
    public function request(string $method, string $url, array $options = []): void
    {
        $response = $this->client->request($method, $url, $options);
        if (!in_array($response->getStatusCode(), [Response::HTTP_OK, Response::HTTP_CREATED], true)) {
            return;
        }
        $content = $response->getContent();
        $decoded = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $this->responseCode = $decoded['web_url'] ?? '';
    }

    /**
     * @return string
     */
    public function getResponse(): string
    {
        return $this->responseCode;
    }
}
