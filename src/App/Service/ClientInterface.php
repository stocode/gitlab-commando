<?php declare(strict_types=1);

namespace App\Service;

interface ClientInterface
{
    public function getResponse(): string;

    public function request(string $method, string $url, array $options = []): void;
}
