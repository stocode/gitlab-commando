<?php

namespace App\Command;

use App\Library\Emoji;
use App\Service\Client;
use App\Service\ClientInterface;
use InvalidArgumentException;
use Symfony\Component\{Console\Input\InputArgument,
    Console\Style\SymfonyStyle,
    Console\Command\Command,
    Console\Input\InputInterface,
    Console\Output\OutputInterface};

class MergeDownCommand extends Command
{
    private ?ClientInterface $client = null;

    protected function configure(): void
    {
        $this->setName('merge:down')
            ->addArgument('target', InputArgument::IS_ARRAY)
            ->setDescription('Creates merge requests from master to some branch.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $authBearer = $_ENV['GITLAB_AUTH_BEARER'];
        $repo = $_ENV['GITLAB_REPO'];
        if (empty($authBearer) || empty($repo)) {
            throw new InvalidArgumentException('$_ENV variables not correct.');
        }
        $targets = !empty($arg = (array) $input->getArgument('target')) ? $arg : ['feature'];
        $io = new SymfonyStyle($input, $output);
        foreach ($targets as $target) {
            $io->text(sprintf('<fg=blue> %s [ INFO ]</> Create merge request from master to %s', Emoji::random(), $target));
            try {
                $this->getClient()->request(
                    'POST',
                    $repo,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                        'auth_bearer' => $authBearer,
                        'body' => '{
                            "id": "stocode",
                            "source_branch": "' . $target . '",
                            "target_branch": "master", 
                            "title": "Merge master into ' . $target . '"
                            }',
                    ]
                );
                $io->text($this->getResponseMessage());
                $io->newLine();
            } catch (\Exception) {
                return self::FAILURE;
            }
        }

        return self::SUCCESS;
    }

    /**
     * @return ClientInterface
     */
    protected function getClient(): ClientInterface
    {
        if ($this->client === null) {
            $this->client = new Client();
        }

        return $this->client;
    }

    private function getResponseMessage(): string
    {
        $response = $this->getClient()->getResponse();
        if ($response !== '') {
            return sprintf('<fg=green> %s [ SUCCESS ]</> Created merge request as <fg=cyan>%s</>', '✅', $response);
        }

        return sprintf('<error> %s [ ERROR ] Something went wrong </error>', '😭');
    }
}
