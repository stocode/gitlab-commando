# stocode Gitlab Commando

## Info
A sample gitlab cli for personal use.

# Examples
```shell
php bin/console merge:down feature/f3 feature/f4
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
